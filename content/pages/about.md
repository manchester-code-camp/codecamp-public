---
title: About
seo:
  title: About
  description: This is the about us page
  extra:
    - name: og:type
      value: website
      keyName: property
    - name: og:title
      value: About
      keyName: property
    - name: og:description
      value: This is the about us page
      keyName: property
    - name: og:image
      value: images/feature-3.jpg
      keyName: property
      relativeUrl: true
    - name: twitter:card
      value: summary_large_image
    - name: twitter:title
      value: About
    - name: twitter:description
      value: This is the about us page
    - name: twitter:image
      value: images/feature-3.jpg
      relativeUrl: true
layout: page
---

Manchester Code Camp is a software development event aimed towards trying to get everyone into software development, but also a specific focus is being placed into trying to get women into software development jobs. We're working with people of all genders within the industry to find out how we can benefit everyone.

The workshops will focus mainly on Full Stack Web Development. This is the predominant and popular track in the software industry in developing websites and software applications. Split between "front end" and "back end", the course will go into the basics of getting up and running as a developer for web applications.