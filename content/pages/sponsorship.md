---
title: Sponsorship
seo:
  title: Sponsorship
  description: Sponsor Manchester Code Camp
  extra:
    - name: og:type
      value: website
      keyName: property
    - name: og:title
      value: Sponsorship
      keyName: property
    - name: og:description
      value: Sponsor Manchester Code Camp
      keyName: property
    - name: twitter:card
      value: summary
    - name: twitter:title
      value: Sponsorship
    - name: twitter:description
      value: Sponsor Manchester Code Camp
layout: page
---

Please contact us at sponsorships@codecamp.community if you wish to sponsor this event.