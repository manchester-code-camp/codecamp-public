---
title: Home
sections:
  - type: hero_section
    title: Get your start in coding
    subtitle: A code boot camp in Manchester for beginners
    content: >-
      **Coming March 2022**
    actions:
      - label: Get Your Ticket Today!
        url: /about
        style: primary
    align: center
    padding_top: large
    padding_bottom: large
    background_color: primary
    background_image: images/hero-background.jpg
    background_image_opacity: 15
    background_image_size: cover
  - type: grid_section
    grid_items:
      - title: 2 Days
        title_align: left
        content: >-
          A two-day in-person and virtual event for new people wanting to learn web development.
        content_align: left
        image: images/2-days.svg
        image_alt: Section item 1 icon
        image_position: left
        image_width: twenty-five
      - title: 2 Tracks
        title_align: left
        content: >-
          Focused on web development and the software industry
        content_align: left
        image: images/2-tracks.svg
        image_alt: Section item 2 icon
        image_position: left
        image_width: twenty-five
      - title: 3 instructors
        title_align: left
        content: >-
          Well-experienced instructors will give you experience and tuition in software development.
        content_align: left
        image: images/20-speakers.svg
        image_alt: Section item 3 icon
        image_position: left
        image_width: twenty-five
    grid_cols: three
    grid_gap_horiz: medium
    grid_gap_vert: medium
    enable_cards: false
    align: center
    background_color: secondary
  - type: features_section
    title: An event for everyone
    features:
      - content: >-
          Manchester Code Camp is a software development event aimed towards trying to get everyone into software development, but also
          a specific focus is being placed into trying to get women into software development jobs. We're working with people of all genders
          within the industry to find out how we can benefit everyone.
        align: center
    feature_padding_vert: medium
    align: center
    padding_top: medium
    padding_bottom: medium
    background_color: none
  - type: features_section
    features:
      - title: Hands-On Workshops
        content: >-
          The workshops will focus mainly on Full Stack Web Development. This is the predominant and popular track in the software industry in 
          developing websites and software applications. Split between "front end" and "back end", the course will go into the basics of getting up and 
          running as a developer for web applications.
        image: images/feature-1.jpg
        image_alt: Feature 1 placeholder image
        media_position: right
        media_width: sixty
      - title: Advice from those currently in the industry
        content: >-
          Our instructors are also currently active and employed within the software industry, and will provide answers to any questions you want to ask, 
          providing specifically a human perspective of what it's like to work in the industry, job expectations, and ultimately how your career might look
          if you choose to get into software development.
        image: images/feature-2.jpg
        image_alt: Feature 2 placeholder image
        media_position: left
        media_width: sixty
    feature_padding_vert: large
    align: center
    padding_top: none
    background_color: none
  - type: cta_section
    title: Keep up-to-date so you don't miss out! There are only a limited amount of available places.
    actions:
      - label: Keep up-to-date
        url: /register-interest
        style: primary
        has_icon: true
        icon: arrow-right
        icon_position: right
    actions_position: right
    actions_width: fourty
    align: left
    padding_top: large
    padding_bottom: large
    background_color: primary
    background_image: images/cta-background.png
    background_image_opacity: 50
  - type: grid_section
    title: Speakers & Instructors
    subtitle: Fantastic lineup of experts!
    grid_items:
      - title: Razwan Choudary
        subtitle: Co-founder, Manchester Code Camp
        content: |-
          Raz, as he is more commonly known, or "Raz Dynamics" is the go-to-guy when it comes to all things Microsoft Dynamic CRM
          and is a Microsoft MVP. He's been in the industry for several decades and has a wide range of experience integrating
          systems for large enterprises.
        image: images/avatar_1.jpg
        image_alt: Speaker 1 photo
        image_position: top
      - title: Jordan Craw
        subtitle: Co-founder, Manchester Code Camp
        content: |-
          Jordan has worked in the technology sector for over 10 years, and has worked as a freelance developer, a developer for several
          Free and Open Source (FOSS) software projects, as well as working as a software developer and technologist. He has his own FOSS
          project, Dynastra Tabletop, which is a Tabletop gaming software.
        image: images/avatar_2.jpg
        image_alt: Speaker 2 photo
        image_position: top
    grid_cols: four
    grid_gap_horiz: medium
    grid_gap_vert: large
    align: center
    background_color: none
  - type: grid_section
    title: Frequently Asked Questions
    subtitle: Our vision
    grid_items:
      - title: What do I need to know to attend?
        title_align: left
        content: >-
          The event is mainly for people who already are somewhat technical in using computers, and know about how to manage their own PC or laptop, install
          programs and so on. Some basic knowledge of dealing with HTML and using text editors is preferable, as well as basic knowledge of web browsers.
        content_align: left
      - title: Where will it be held?
        title_align: left
        content: >-
          The event will be held both in-person in Manchester, United Kingdom, and we are planning to stream the event to a limited amount of online participants.If COVID restrictions increase in the UK then the event will be moved online.
        content_align: left
      - title: How long is the event?
        title_align: left
        content: >-
          The event will take place over two days in March.
        content_align: left
    grid_cols: two
    grid_gap_horiz: medium
    grid_gap_vert: small
    enable_cards: false
    align: center
    padding_top: large
    padding_bottom: large
    background_color: secondary
    background_image: images/faq-background.svg
    background_image_repeat: repeat
    background_image_size: auto
    background_image_opacity: 15
  - type: grid_section
    grid_items:
      - title: Address
        content: |-
          Manchester<br />
          United Kingdom<br />
          [info@manchestercodecamp.community](mailto:info@manchestercodecamp.community)<br/>
      - title: Follow Us
        content: |-
          [Youtube](https://www.youtube.com/)<br/>
          [Medium](https://medium.com/)<br/>
          [Facebook](https://www.facebook.com/)<br/>
          [Twitter](https://twitter.com/home)<br/>
    grid_cols: two
    grid_gap_horiz: medium
    grid_gap_vert: large
    enable_cards: false
    align: center
    padding_top: medium
    padding_bottom: medium
    background_color: none
seo:
  title: Manchester Code Camp
  description: The Manchester Code Camp Event for beginners introduction to software development.
  extra:
    - name: og:type
      value: website
      keyName: property
    - name: og:title
      value: Manchester Code Camp
      keyName: property
    - name: og:description
      value: The preview of the Event theme
      keyName: property
    - name: og:image
      value: images/feature-3.jpg
      keyName: property
      relativeUrl: true
    - name: twitter:card
      value: summary_large_image
    - name: twitter:title
      value: Manchester Code Camp
    - name: twitter:description
      value: The Manchester Code Camp Event for beginners introduction to software development.
    - name: twitter:image
      value: images/feature-3.jpg
      relativeUrl: true
layout: advanced
---
